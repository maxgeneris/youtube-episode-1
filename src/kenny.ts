import { serialize } from "@thi.ng/hiccup";

const rep = (n:number, fn:(i:number)=> any) => 
    Array.from({length:n}, (_, i)=> fn(i));

const range = (min:number, max:number, step=1) =>
    Array.from({length: Math.floor((max-min)/step)}, (_,i)=> min + i*step);

const grid = (cols:number, rows:number) =>
    range(0,cols).flatMap(c => range(0, rows).map(r => [c,r]));




const rFloat = (min:number, max:number) => min + Math.random()*(max-min);
const rInt = (min:number, max:number) => 
    Math.floor(min + Math.random()*(max-min));
const rPick = (arr:any[]) => arr[rInt(0, arr.length)];

const hues = rep(rInt(2, 6), () => rInt(0,360));



const W = 600, H = 800, S = rInt(30,60);

const columns = Math.floor(0.94*W/S), rows = Math.floor(0.94*H/S);


const cell =(c:number, r:number) => {
    const x = c*S -columns*S/2;
    const y = r*S - rows*S/2;
    const h = rPick(hues);

    const rectPoints = [[x,y], [x+S, y], [x+S, y+S], [x, y+S]];
    const triPoints = rectPoints.toSpliced(rInt(0,4), 1);

    const lineCount = rInt(7, 20);

    return ["g", {
        "stroke-linejoin":"round",
        "stroke-width":1.2,
        stroke: `hsl(${h}, ${rInt(10, 50)}%, ${rInt(20,70)}%)`,
        "transform-origin": [x+S/2,y+S/2]
    },
       
    ["animateTransform", {
        attributeName:"transform",
        type:"rotate",
        from:0,
        to:360,
        dur:"4s",
        repeatCount:"indefinite"
    }],
        ["polygon", {
            points: triPoints, 
            fill:`hsl(${h}, ${rInt(10, 40)}%, ${rInt(10,50)}%)`
        }],
        ["polygon", {
            points: rectPoints, 
            fill:"none"
        }],
        rep(rPick([lineCount, 0, 0]), (i)=> [
            "line", {
                "stroke-width":0.5,
                x1: x, 
                x2: x+S,
                y1: y + i*S/lineCount,
                y2: y + i*S/lineCount,
            }
        ]),
        rep(rPick([lineCount, 0, 0]), (i)=> [
            "line", {
                "stroke-width":0.5,
                x1: x + i*S/lineCount, 
                x2: x + i*S/lineCount,
                y1: y,
                y2: y +S,
            }
        ])
    ]
}



const svgTree = ["svg",
    {
        width:W,
        height:H,
        viewBox: `${-W/2} ${-H/2} ${W} ${H}`,
        // xmlns:'http://www.w3.org/2000/svg', 
        // "xmlns:xlink":'http://www.w3.org/1999/xlink',
    },

    ["rect", {
        x:-W/2, y:-H/2,
        width:W, height:H,
        fill:`hsl(${rPick(hues)}, 50%, 8%)`
    }],
    ["g", {id:"fg", filter:"url(#glow)"},
        grid(columns, rows).map (([c,r]) => cell(c,r))

    ]

];



document.body.insertAdjacentHTML("beforeend", serialize(svgTree));



